
#include <omnetpp.h>
#include "artery/rosomnet/NetPkt_m.h"
#include "artery/rosomnet/TelnetPkt_m.h"

#include "artery/rosomnet/CaObject.h"
#include "artery/rosomnet/RobotMiddleware.h"
#include "artery/rosomnet/Middleware.h"
#include "artery/rosomnet/VehicleDataProvider.h"
#include "artery/rosomnet/MobilityROS.h"
#include "artery/traci/ControllableVehicle.h"
#include "artery/traci/MobilityBase.h"

#include "inet/common/ModuleAccess.h"
#include <vanetza/common/position_fix.hpp>
#include "artery/rosomnet/ROSOMNeT.h"
#include "inet/mobility/base/MovingMobilityBase.h"
#include "artery/rosomnet/Asn1PacketVisitor.h"

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <ros_its_msgs/CAM_simplified.h>
#include <omnetpp/clistener.h>
#include <omnetpp/csimplemodule.h>

#include <fstream>
#include <chrono>

using namespace omnetpp;
using namespace ros;
using namespace std;

static const simsignal_t scSignalCamReceived = cComponent::registerSignal("CamReceived");

namespace artery
{

class RSU : public cSimpleModule, public cListener
    
{
  public:
	RSU();
	int addr;
	int srvAddr;
	VehicleDataProvider mVehicleDataProvider;
        
  protected:
    virtual void initialize() override;
    virtual void handleMessage(omnetpp::cMessage *msg) override;
    void receiveSignal(cComponent*, omnetpp::simsignal_t, cObject*, cObject* = nullptr) override;
    void initializeManagementInformationBase(vanetza::geonet::MIB&);
   
};

Define_Module(RSU);

void RSU::initialize()
{
    addr = par("addr");
    srvAddr = par("srvAddr");
    cout << "Rsu initialized" << endl;
    //findHost()->subscribe(INET_API::MobilityBase::stateChangedSignal, this);
    //getFacilities().register_const(&mVehicleDataProvider);
    //findHost()->subscribe(scSignalCamReceived,this);
}

void RSU::handleMessage(cMessage *msg)
{
    // determine destination address
    //NetPkt *pkt = check_and_cast<NetPkt *>(msg);
    //int dest = pkt->getDestAddress();
    //EV << "Relaying packet to addr=" << dest << endl;

    // send msg to destination after the delay
    /*TelnetPkt *telnetPkt = new TelnetPkt("x");
    telnetPkt->setPayload("x");
    telnetPkt->setDestAddress(1);
    telnetPkt->setSrcAddress(1);
    //send(telnetPkt, "g$o",0);
    NetPkt *pkt = check_and_cast<NetPkt *>(telnetPkt);
    send(pkt, "g$o",0);*/
    
}
void RSU::initializeManagementInformationBase(vanetza::geonet::MIB& mib)
{
	using vanetza::geonet::StationType;
	Middleware::initializeManagementInformationBase(mib);

	mGnStationType = StationType::PASSENGER_CAR;
	mVehicleDataProvider.setStationType(mGnStationType);

}

void RSU::receiveSignal(cComponent* source, simsignal_t signal, cObject *obj,cObject*)
{
    if (signal == scSignalCamReceived) {
	auto* cam = dynamic_cast<CaObject*>(obj);
	if (cam){
   
	    double Speed = cam->asn1()->cam.camParameters.highFrequencyContainer.choice.basicVehicleContainerHighFrequency.speed.speedValue;
		Speed = Speed * 0.01;
		double Heading = cam->asn1()->cam.camParameters.highFrequencyContainer.choice.basicVehicleContainerHighFrequency.heading.headingValue;
		Heading = (Heading * M_PI / 180.00000) - M_PI ; //to rad * 0.1
		double latitude = cam->asn1()->cam.camParameters.basicContainer.referencePosition.latitude / 1000000.000 ;
		//latitude = latitude * M_PI / 180;
		double longitude = cam->asn1()->cam.camParameters.basicContainer.referencePosition.longitude / 1000000.000;
		//longitude = longitude * M_PI / 180;
		double altitude = cam->asn1()->cam.camParameters.basicContainer.referencePosition.altitude.altitudeValue;
	    cout << "Cam receive by RSU" << endl;
	}
    }
	    EV_WARN << "CAM RECEPTION" << std::endl;
	    TelnetPkt *telnetPkt = new TelnetPkt("x");
	    telnetPkt->setPayload("x");
	    telnetPkt->setDestAddress(1);
	    telnetPkt->setSrcAddress(2);
	    NetPkt *pkt = check_and_cast<NetPkt *>(telnetPkt);
	    send(pkt, "g$o",0);
}
}
