/*
 * Artery V2X Simulation Framework
 * Copyright 2014-2017 Raphael Riebl
 * Licensed under GPLv2, see COPYING file for detailed license and warranty terms.
 */

//#ifndef ARTERY_ROBOTMIDDLEWARE_H_SYJDG2DX
//#define ARTERY_ROBOTMIDDLEWARE_H_SYJDG2DX

#include <string>
#include <queue>

#include "artery/rosomnet/Middleware.h"
#include "artery/rosomnet/VehicleDataProvider.h"
#include "artery/rosomnet/MobilityROS.h"

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <omnetpp.h>
#include <ros_its_msgs/CAM_simplified.h>
#include "artery/rosomnet/ROSOMNeT.h"

#include "artery/rosomnet/Facilities.h"
#include "artery/rosomnet/LocalDynamicMap.h"
#include "artery/rosomnet/Timer.h"
#include "artery/utility/Identity.h"
#include <omnetpp/clistener.h>
#include <omnetpp/csimplemodule.h>
#include <omnetpp/simtime.h>
#include <vanetza/access/data_request.hpp>
#include <vanetza/access/interface.hpp>
#include <vanetza/btp/data_interface.hpp>
#include <vanetza/btp/port_dispatcher.hpp>
#include <vanetza/common/clock.hpp>
#include <vanetza/common/stored_position_provider.hpp>
#include <vanetza/common/runtime.hpp>
#include <vanetza/dcc/flow_control.hpp>
#include <vanetza/dcc/scheduler.hpp>
#include <vanetza/dcc/state_machine.hpp>
#include <vanetza/geonet/packet.hpp>
#include <vanetza/geonet/router.hpp>
#include <vanetza/security/backend.hpp>
#include <vanetza/security/certificate_cache.hpp>
#include <vanetza/security/certificate_provider.hpp>
#include <vanetza/security/certificate_validator.hpp>
#include <vanetza/security/security_entity.hpp>
#include <vanetza/security/sign_header_policy.hpp>
#include <map>
#include <memory>



using namespace ros;
using namespace std;
class RadioDriverBase;

namespace artery
{

class RSU : public Middleware
{
	public:
		RSU();
		void initialize(int stage) override;
		void finish() override;
		void setPosition(double x, double y, double z);
		void receiveSignal(cComponent*, omnetpp::simsignal_t, cObject*, cObject* = nullptr) override;
		//ros::Publisher pub_RX_omnet;

	protected:
		void initializeIdentity(Identity&) override;
		void initializeManagementInformationBase(vanetza::geonet::MIB&) override;
		void update() override;

	private:

	const long TOPIC_QUEUE_LENGTH = 1000;
	const double PACKET_TRANSMIT_INTERVAL = 0.010;

	//void RXNetCallback(const ros_its_msgs::CAM_simplified& msg);
	//void RXNetCallback(const ros_its_msgs::OMNET_CAM& msg);
	ROSOMNeT& rosomnet;

		static int instanceCounter;
		const string nameSpace;

		//static int ii;

		string car_name;
		double ros_x;
		double ros_y;
		double ros_z;
		double heading;

		void initializeVehicleController();
		void updatePosition();
	
		//Subscriber RXNetSubscriber;

		//traci::VehicleController* mVehicleController;
		//VehicleDataProvider mVehicleDataProvider;
		//INET_API::inet::IMobility* getMobilityModule();
		//MobilityROS* getMobilityModule();

		RadioDriverBase* mRadioDriver;
		omnetpp::cGate* mRadioDriverIn;
		omnetpp::cGate* mRadioDriverOut;
};

} // namespace artery


