# CMake generated Testfile for 
# Source directory: /home/diogo/catkin_ws/src
# Build directory: /home/diogo/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("CISTER_car_simulator/src/mqtt_bridge-master")
subdirs("vision_opencv/opencv_tests")
subdirs("CISTER_car_simulator/src/prius_msgs")
subdirs("CISTER_car_simulator/src/ros_its_msgs")
subdirs("vision_opencv/vision_opencv")
subdirs("beginner_tutorials")
subdirs("vision_opencv/cv_bridge")
subdirs("vision_opencv/image_geometry")
subdirs("opencv")
subdirs("teste")
subdirs("CISTER_car_simulator/src/car_demo")
subdirs("CISTER_car_simulator/src/prius_description")
subdirs("xacro")
