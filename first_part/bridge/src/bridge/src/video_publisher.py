#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

import numpy as np

import cv2

import rospy

#import camera_info_manager
from sensor_msgs.msg import Image

from cv_bridge import CvBridge, CvBridgeError

bridge = CvBridge()

def main():
    """Publish a video as ROS messages.
    """

    # Set up node.
    rospy.init_node("video_publisher", anonymous=True)
    pub = rospy.Publisher("video", Image, queue_size=10)

    # Open video.
    video = cv2.VideoCapture(0)
	
    while True:
    # Loop through video frames.
    #while not rospy.is_shutdown():
    	ret, frame = video.read()
    	frame = np.array(frame)
    #while True:
    	image_message = bridge.cv2_to_imgmsg(frame, encoding = "passthrough")
    	pub.publish(image_message)

    return

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass

viedo.release()
cve.destroyAllWindows()
