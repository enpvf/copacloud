import numpy as np
import pandas as pd
import time
import torch
import torch.nn as nn
import cv2
from utility import *
from yolo import darknet
import random
import argparse
import pickle as pkl
from vidgear.gears import VideoGear


def arg_parse():
    """parsing arguments"""
    parser = argparse.argumentparser(description='yolo v3 real time detection')
    parser.add_argument("--confidence", dest="confidence", help="object confidence to filter predictions", default=0.25)
    parser.add_argument("--nms_thresh", dest="nms_thresh", help="nms threshhold", default=0.4)
    parser.add_argument("--reso", dest='reso', help=
    "input resolution of the network. increase to increase accuracy. decrease to increase speed",
                        default="160", type=str)
    return parser.parse_args()


def prep_image(img, inp_dim):
    """converting a numpy array of frame into pytorch tensor"""
    orig_im = img
    dim = orig_im.shape[1], orig_im.shape[0]
    img = cv2.resize(orig_im, (inp_dim, inp_dim))
    img_ = img[:, :, ::-1].transpose((2, 0, 1)).copy()
    img_ = torch.from_numpy(img_).float().div(255.0).unsqueeze(0)
    return img_, orig_im, dim


def write(x, img):
    c1 = tuple(x[1:3].int())
    c2 = tuple(x[3:5].int())
    cls = int(x[-1])
    label = "{0}".format(classes[cls])
    color = random.choice(colors)
    cv2.rectangle(img, c1, c2, color, 1)
    t_size = cv2.gettextsize(label, cv2.font_hershey_plain, 1, 1)[0]
    c2 = c1[0] + t_size[0] + 3, c1[1] + t_size[1] + 4
    cv2.rectangle(img, c1, c2, color, -1)
    cv2.puttext(img, label, (c1[0], c1[1] + t_size[1] + 4),
                cv2.font_hershey_plain, 1, [225, 255, 255], 1);
    return img


if __name__ == "__main__":
    cfgfile = "cfg/yolov3.cfg"
    weightsfile = "weight/yolov3.weights"
    num_classes = 80

    args = arg_parse()
    confidence = float(args.confidence)
    nms_thesh = float(args.nms_thresh)
    start = 0

    num_classes = 80
    bbox_attrs = 5 + num_classes

    model = darknet(cfgfile).to(device)
    model.load_weights(weightsfile)

    model.network_info["height"] = args.reso
    inp_dim = int(model.network_info["height"])

    assert inp_dim % 32 == 0
    assert inp_dim > 32

    model.eval()

    cap = videogear(source='input.mp4').start()

    #assert cap.isopened(), 'cannot capture source'

    frames = 0
    start = time.time()
    while True:

         frame = cap.read()

         img, orig_im, dim = prep_image(frame, inp_dim)
         img.to(device)

         output = model(img)
         output = write_results(output, confidence, num_classes, nms_conf=nms_thesh)

         output[:, 1:5] = torch.clamp(output[:, 1:5], 0.0, float(inp_dim)) / inp_dim

         output[:, [1, 3]] *= frame.shape[1]
         output[:, [2, 4]] *= frame.shape[0]

         classes = load_classes('data/coco.names')
         colors = pkl.load(open("color/pallete", "rb"))

         list(map(lambda x: write(x, orig_im), output))

         cv2.imshow("frame", orig_im)
         key = cv2.waitkey(1)
         if key & 0xff == ord('q'):
            break

