#!/usr/bin/env python3

import cv2 as cv
import numpy as np
import socket
import sys
import pickle
import struct
import time
import rospy
import roslib
import threading
from datetime import datetime
from sensor_msgs.msg import Image

from cv_bridge import CvBridge, CvBridgeError

bridge = CvBridge()
#from vidgear.gears import VideoGear

#cap = VideoGear(source='input.mp4').start()

clientsocket=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
clientsocket.connect(('192.168.1.100',8089))#edge
#clientsocket.connect(('23.98.144.136',8089))#cloud
rospy.init_node("video_subscriber", anonymous=True)

def thread_socket():
	byte_recv= clientsocket.recv(1024)
	label=byte_recv.decode()
	if (label==""):
		print("nada detetado")
	else:
		print (label)


if __name__ == "__main__":
	#rospy.init_node("video_subscriber", anonymous=True)
	#def __init__(self):
		    
	def callback(data):
		frame = bridge.imgmsg_to_cv2(data,'bgr8')

		data = pickle.dumps(frame)
		message_size = struct.pack("L", len(data))
		clientsocket.sendall(message_size + data)
		t=threading.Thread(target=thread_socket)
		t.start()
		
		
	rospy.Subscriber("/car1/front_camera/image_raw",Image,callback)
	rospy.spin()
