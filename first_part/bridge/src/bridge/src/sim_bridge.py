#!/usr/bin/env python3

import cv2 as cv
import numpy as np
import socket
import sys
import pickle
import struct
import time
import rospy
import roslib
import threading
from datetime import datetime
from sensor_msgs.msg import Image
from std_msgs.msg import String
#from image_processing.msg import drive_param

from cv_bridge import CvBridge, CvBridgeError

bridge = CvBridge()
#from vidgear.gears import VideoGear

#cap = VideoGear(source='input.mp4').start()

clientsocket=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
clientsocket.connect(('192.168.1.100',8089))#edge
#clientsocket.connect(('23.98.144.136',8089))#cloud
rospy.init_node("object_detection", anonymous=True)

pub = rospy.Publisher('car1/sinal', String, queue_size=1)	#sinal detetado


if __name__ == "__main__":
		    
	def callback(data):
		frame = bridge.imgmsg_to_cv2(data,'bgr8')

		data = pickle.dumps(frame)
		message_size = struct.pack("L", len(data))
		clientsocket.sendall(message_size + data)
		now=datetime.now()
		time1=now.strftime("%S.%f")
		time1=float(time1)
		byte_recv= clientsocket.recv(1024)
		now=datetime.now()
		time2=now.strftime("%S.%f")
		time2=float(time2)
		label=byte_recv.decode()
		msg=String()
		msg.data=label
		pub.publish(msg)
		
		
	rospy.Subscriber("/car1/front_camera/image_raw",Image,callback)
	rospy.spin()
