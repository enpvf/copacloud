#! /usr/bin/env python

import sys
import rospy
import cv2
import numpy as np

from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

class image_converter:
		#--- Publisher of the edited frame
	image_pub = rospy.Publisher("image_topic",Image,queue_size=10)
	bridge = CvBridge()
	cap = cv2.VideoCapture(0)
	while True:
		frame = cap.read()
		#frame = np.array(frame)
		#try:
		    #cv_image = bridge.imgmsg_to_cv2(frame, "bgr8")
		#except CvBridgeError as e:
		#    print(e)
		#cv2.imshow("Image window", frame)
        	#cv2.waitKey(3)
        	#--- Publish the modified frame to a new topic
            	image_pub.publish(bridge.cv2_to_imgmsg(frame, "bgr8"))
     


def main(args):
    #--- Create the object from the class we defined before
    ic = image_converter()
    
    #--- Initialize the ROS node
    rospy.init_node('image_converter', anonymous=True)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
        
    #--- In the end remember to close all cv windows
    cv2.destroyAllWindows()

if __name__ == '__main__':
        main(sys.argv)
