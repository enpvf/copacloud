import cv2 as cv
import numpy as np
import socket
import sys
import pickle
import struct
import time
from datetime import datetime
from vidgear.gears import VideoGear
i=0
cap = VideoGear(source='input.mp4').start()
clientsocket=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
#clientsocket.connect(('192.168.1.4',8089))#edge
clientsocket.connect(('23.98.144.136',8089))#cloud

def write_lat(lat):
	f=open("lat.txt","a")
	f.write(str(lat*1000)+"\n")
	f.close

while True:
	frame=cap.read()
	data = pickle.dumps(frame)
	message_size = struct.pack("L", len(data))
	clientsocket.sendall(message_size + data)
	now=datetime.now()
	time1=now.strftime("%S.%f")
	time1=float(time1)
	
	lines=clientsocket.recv(1024)

	now=datetime.now()
	time2=now.strftime("%S.%f")
	time2=float(time2)
	
	lat=time2-time1
	write_lat(lat)
	lines_arr=pickle.loads(lines)
	print(repr(lines_arr))
	i+=1
	if (i==750):
		print(i)
		clientsocket.close()
